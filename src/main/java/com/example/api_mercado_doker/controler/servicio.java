package com.example.api_mercado_doker.controler;

import com.example.api_mercado_doker.model.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("")
public class servicio {
    //Usuario Cajero (5)
    //Usuario Reponedor (3)
    //Producto (8)
    //Merma (3)


    private static List<Usuario_Cajero> usuarios_caj = new ArrayList<>();
    private static List<Usuario_Reponedor> usuarios_rep = new ArrayList<>();
    private static List<Producto> productos = new ArrayList<>();
    private static List<Merma> merma = new ArrayList<>();

    @GetMapping("")
    public String mostrar_home() {
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>home api</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<style>\n" +
                "body {background-color:black; color:white; }"+
                "</style>"+
                "<h1  style='a'>home api super mercado</h1>\n" +
                "<ul type=”A”>\n" +
                "    <li><a href=\"cajeros\">informacion de cajeros</a></li>\n" +
                "    <li><a href=\"reponedores\">informacion de reponedores</a></li>\n" +
                "    <li><a href=\"productos\">informacion de productos</a></li>\n" +
                "    <li><a href=\"merma\">informacion de merma</a></li>\n" +
                "</ul>"+
                "</body>\n" +
                "</html>";
    }

    @GetMapping("/cajeros")
    public List<Usuario_Cajero>  datos_usuarios_caj() {
        usuarios_caj.add(new Usuario_Cajero("pablo", "20.456.222-2", "pablo1234", 16));
        usuarios_caj.add(new Usuario_Cajero("juan", "20.456.333-2", "juan1234", 23));
        usuarios_caj.add(new Usuario_Cajero("pedro", "20.456.444-2", "pedro1234", 29));
        usuarios_caj.add(new Usuario_Cajero("matias", "20.456.555-2", "matias1234", 9));
        usuarios_caj.add(new Usuario_Cajero("alexis", "20.456.666-2", "alexis1234", 14));

        return usuarios_caj;
    }
    @GetMapping("/reponedores")
    public List datos_usuarios_rep() {
        usuarios_rep.add(new Usuario_Reponedor("pancho", "16.345.123-1", "pancho1234", "45-A"));
        usuarios_rep.add(new Usuario_Reponedor("pepe", "16.345.333-2", "pepe1234", "49-A"));
        usuarios_rep.add(new Usuario_Reponedor("marco", "16.345.333-3", "marco1234", "04-b"));
        return usuarios_rep;
    }
    @GetMapping("/productos")
    public List datos_productos() {
        productos.add(new Producto("leche 1L", "45-A", 55, "pasillo 1"));
        productos.add(new Producto("hamburguesas 150g", "45-b", 25, "pasillo 5"));
        productos.add(new Producto("galletas", "25-A", 60, "pasillo 7"));
        productos.add(new Producto("barra chocolate", "25-b", 35, "pasillob7"));
        productos.add(new Producto("queso 1kg", "35-A", 20, "pasillo 1"));
        productos.add(new Producto("Ron 750ccL", "02-B", 30, "pasillo 4"));
        productos.add(new Producto("carne molida", "08-A", 55, "pasillo 5"));
        productos.add(new Producto("arroz 1kg", "05-A", 70, "pasillo 3"));
        return productos;
    }

    @GetMapping("/merma")
    public List datos_merma(){
        merma.add(new Merma("leche chocolate","34lk23-LA",50));
        merma.add(new Merma("lapis","50jd23-LA",50));
        merma.add(new Merma("paltas","43hg54-US",50));
        return merma;
    }

}
