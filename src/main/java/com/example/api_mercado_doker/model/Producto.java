package com.example.api_mercado_doker.model;

public class Producto {
    //Producto: Nombre, sección, stock, precio, ubicación

    private String nombre;
    private String sección;
    private int stock;
    private String ubicación;

    public Producto(String nombre , String sección , int stock , String ubicación) {
        this.nombre = nombre;
        this.sección = sección;
        this.stock = stock;
        this.ubicación = ubicación;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSección() {
        return sección;
    }

    public void setSección(String sección) {
        this.sección = sección;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getUbicación() {
        return ubicación;
    }

    public void setUbicación(String ubicación) {
        this.ubicación = ubicación;
    }
}
