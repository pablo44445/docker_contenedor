package com.example.api_mercado_doker.model;

public class Merma {
    //Merma (3): Nombre, motivo, cantidad

    private String nombre;
    private String modelo;
    private int cantidad;

    public Merma(String nombre,String modelo , int cantidad) {
        this.nombre = nombre;
        this.modelo = modelo;
        this.cantidad = cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
