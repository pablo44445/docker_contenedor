package com.example.api_mercado_doker.model;

public class Usuario_Cajero {
    //Usuario Cajero (5): Nombre, rut, usuario, contraseña, ventas

    private String nombre;
    private String rut;
    private String contraseña;
    private int ventas;

    public Usuario_Cajero(String nombre , String rut , String contraseña , int ventas){
        this.nombre = nombre;
        this.rut = rut;
        this.contraseña = contraseña;
        this.ventas = ventas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public int getVentas() {
        return ventas;
    }

    public void setVentas(int ventas) {
        this.ventas = ventas;
    }
}
