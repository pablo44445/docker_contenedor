package com.example.api_mercado_doker.model;

public class Usuario_Reponedor {
    //Usuario Reponedor (3): Nombre, rut, usuario, contraseña, sección.

    private String nombre;
    private String rut;
    private String contraseña;
    private String sección;

    public Usuario_Reponedor(String nombre, String rut, String contraseña, String sección){
        this.nombre = nombre;
        this.rut = rut;
        this.contraseña = contraseña;
        this.sección = sección;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getSección() {
        return sección;
    }

    public void setSección(String sección) {
        this.sección = sección;
    }
}
