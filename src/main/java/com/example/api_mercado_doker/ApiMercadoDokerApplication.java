package com.example.api_mercado_doker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiMercadoDokerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiMercadoDokerApplication.class, args);
	}

}
