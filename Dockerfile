FROM openjdk:17
VOLUME /tmp
COPY "./target/api_mercado_doker-0.0.1-SNAPSHOT.jar" "Container_docker.jar"
EXPOSE 8080
ENTRYPOINT ["java","-jar","Container_docker.jar"]
